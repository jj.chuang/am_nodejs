'use strict';
let fs = require('fs');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var port = process.env.PORT || 80;
var path = require('path');

var events = require('events');
var eventEmitter = new events.EventEmitter();

// 歷史資料發送
var OHLCVEventHandler = function (OHLCV) {
    var jsonObj = [];
    var tmp_values = [];
    for (var i = 0; i < OHLCV.length; i++) {
        for (var j = 0; j < OHLCV[i].length; j++) {
            if (j == 0) {
                tmp_values.push({ date: OHLCV[i][j] }); //key
            }
            else if (j == 1) {
                tmp_values.push({ open: OHLCV[i][j] });
            }
            else if (j == 2) {
                tmp_values.push({ high: OHLCV[i][j] });
            }
            else if (j == 3) {
                tmp_values.push({ low: OHLCV[i][j] });
            }
            else if (j == 4) {
                tmp_values.push({ close: OHLCV[i][j] });
            }
            else if (j == 5) {
                tmp_values.push({ volumn: OHLCV[i][j] });
            }
            // console.log("i:" + i + "/j:" + j + "/" + label + "/" + OHLCV[i][j]);
        }
        jsonObj[i] = tmp_values;
        tmp_values = [];
    }
    
    var str = JSON.stringify(jsonObj);
    console.log('OHLCV:\n' + str);

    io.emit('OHLCV', jsonObj);
}

// ticker data event handler:
var tickerEventHandler = function (ticker) {
    var str = JSON.stringify(ticker);
    console.log('ticker:\n' + str);
    
    // var json = JSON.parse(ticker);
    io.emit('ticker', ticker);
}

// 委託簿 事件觸發處理 (沒在用)
var orderBookEventHandler = function (orderBook) {
    var str = JSON.stringify(orderBook);
    console.log('orderBook:\n' + str);

    // io.emit('orderBook', orderBook);
}

// Assign the event handler to an event:
eventEmitter.on('OHLCV', OHLCVEventHandler);
eventEmitter.on('ticker', tickerEventHandler);
eventEmitter.on('orderBook', orderBookEventHandler);


let ccxt = require('ccxt');
// console.log(ccxt.exchanges); // print all available exchanges

let delay = 2000; // milliseconds = seconds * 1000
// let delay = 20000;
(async () => {
    // let kraken = new ccxt.kraken({ verbose: false }); // log HTTP requests
    let bitfinex = new ccxt.bitfinex({ verbose: false });

    // await kraken.load_markets() // request markets
    // console.log(kraken.id, kraken.markets)    // output a full list of all loaded markets
    // console.log(Object.keys(kraken.markets)) // output a short list of market symbols
    // console.log(kraken.markets['BTC/USD'])    // output single market details

    if (bitfinex.hasFetchOHLCV) {
        (async () => {
            var OHLCV = await bitfinex.fetchOHLCV('BTC/USD', '1m');  // 1m, 5m, 15m, 1d, 1M 經測試可用
            console.log(OHLCV);    
            eventEmitter.emit('OHLCV', OHLCV);
        })()
    }
            
    while (false) {
        // console.log(kraken.id, await kraken.fetchOrderBook(kraken.symbols[7]), {
        
        // let orderBook = await kraken.fetchOrderBook('BTC/USD')
        let orderBook = await bitfinex.fetchOrderBook('BTC/USD')
        /*
        console.log(orderBook, {
            'limit_bids': 5, // max = 50
            'limit_asks': 5, // may be 0 in which case the array is empty
            'group': 1, // 1 = orders are grouped by price, 0 = orders are separate
        });
        */

        // fs.writeFile('kraken_order_book.txt', JSON.stringify(orderBook), function (err) {
        fs.writeFile('bitfinex_order_book.txt', JSON.stringify(orderBook), function (err) {
        // fs.appendFile('kraken_ticker.txt', JSON.stringify(ticker), function (err) {
            if (err) throw err;
            // console.log('Updated!');
        });
        
        eventEmitter.emit('orderBook', orderBook);

        // let ticker = await kraken.fetchTicker('BTC/USD');
        let ticker = await bitfinex.fetchTicker('BTC/USD');
        // console.log(kraken.id, ticker);
        eventEmitter.emit('ticker', ticker);
        // fs.writeFile('kraken_ticker.txt', String(ticker), function (err) {
        /*
        fs.appendFile('kraken_ticker.txt', JSON.stringify(ticker), function (err) {
            if (err) throw err;
            console.log('Updated!');
        });
        */
        await new Promise(resolve => setTimeout(resolve, delay)); // rate limit
    }

    // console.log(await kraken.fetchTrades('BTC/USD'));
    // console.log(await kraken.fetchOHLCV('BTC/USD', '1d'));

    /*
    console.log("RateLimit: " + kraken.rateLimit);
    let sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));
    if (kraken.hasFetchOHLCV) {
        (async () => {
            for (symbol in kraken.markets) {
                await sleep(kraken.rateLimit) // milliseconds
                console.log(await kraken.fetchOHLCV(symbol, '1m')) // one minute
            }
        })()
    }
    */
})()


app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/index2.html'));
});

app.get('/ccxt.js', function (req, res) {
    res.sendFile(path.join(__dirname + '/ccxt.js'));
});

app.get('/kraken_order_book', function (req, res) {
    res.sendFile(path.join(__dirname + '/kraken_order_book.txt'));
});

app.get('/bitfinex_order_book', function (req, res) {
    res.sendFile(path.join(__dirname + '/bitfinex_order_book.txt'));
});

io.on('connection', function (socket) {
    console.log('a user connected');

    socket.on('message', function (msg) {
        console.log('message: ' + msg);
    });

    socket.on('disconnect', function () {
        console.log('user disconnected');
    });
});

http.listen(port, function () {
    console.log('listening on *:' + port);
});
/*
app.listen(port, '127.0.0.1', function () {
    console.log('HTTP server running on http://127.0.0.1/');
});
*/