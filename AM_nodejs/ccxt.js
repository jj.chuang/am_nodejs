'use strict';

function resolveAfter2Seconds(x) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(x);
        }, 3000);
    });
}

async function f1() {
    for (var i = 0; i < 10; i++) {
        var x = await resolveAfter2Seconds(i);
        console.log(x); // 10
        alert(x);
    }
}


async function run_ccxt() {

    // var exchangs = console.log(ccxt.exchanges); // print all available exchanges
    alert(ccxt.exchanges);

    let delay = 2000; // milliseconds = seconds * 1000
    // (async () => {
        let kraken = new ccxt.kraken({ verbose: false }); // log HTTP requests

        alert(kraken.id);
        // await kraken.load_markets(); // request markets
        // let krakenMarkets = await kraken.loadMarkets();
        // console.log(kraken.id, kraken.markets)    // output a full list of all loaded markets
        // console.log(Object.keys(kraken.markets)) // output a short list of market symbols
        // console.log(kraken.markets['BTC/USD'])    // output single market details

        // alert(krakenMarkets);
        while (true) {
            // console.log(kraken.id, await kraken.fetchOrderBook(kraken.symbols[7]), {
            /*
            let orderBook = await kraken.fetchOrderBook('BTC/USD')
            console.log(orderBook, {
                'limit_bids': 5, // max = 50
                'limit_asks': 5, // may be 0 in which case the array is empty
                'group': 1, // 1 = orders are grouped by price, 0 = orders are separate
            });
            */
            alert("hello ticker");
            var ticker = await kraken.fetchTicker('BTC/USD');
            console.log(kraken.id, ticker);
            alert(ticker);
            // fs.writeFile('kraken_ticker.txt', String(ticker), function (err) {
            /*
            fs.appendFile('kraken_ticker.txt', JSON.stringify(ticker), function (err) {
                if (err) throw err;
                console.log('Updated!');
            });
            */
            await new Promise(resolve => setTimeout(resolve, delay)); // rate limit
        }


        // console.log(await kraken.fetchTrades('BTC/USD'));
        // console.log(await kraken.fetchOHLCV('BTC/USD', '1d'));

        /*
        console.log("RateLimit: " + kraken.rateLimit);
        let sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));
        if (kraken.hasFetchOHLCV) {
            (async () => {
                for (symbol in kraken.markets) {
                    await sleep(kraken.rateLimit) // milliseconds
                    console.log(await kraken.fetchOHLCV(symbol, '1m')) // one minute
                }
            })()
        }
        */
    // })()
}