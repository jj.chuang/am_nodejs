'use strict';
var express = require('express');
var app = express();
var port = process.env.PORT || 80;
var path = require('path');


app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/index2.html'));
});

app.get('/ccxt.js', function (req, res) {
    res.sendFile(path.join(__dirname + '/ccxt.js'));
});

app.listen(port, '127.0.0.1', function () {
    console.log('HTTP server running on http://127.0.0.1/');
});
